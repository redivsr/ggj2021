﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesManager : MonoBehaviour
{
    [SerializeField] private string lobbySceneName;
    [SerializeField] private string inventoryServiceSceneName;
    [SerializeField] private string customerServiceSceneName;
    [SerializeField] private GameObject mainCamera;
    public static event Action OnUnload;

    private string currentLevel = "";
    private bool levelLoaded = false;
    
    private List<string> levels;

#region Singleton
    private static ScenesManager instance;

    private void Awake()
    {
        instance = this;
    }
#endregion

    void Start()
    {
        MenuManager.OnBackToMainPressed += UnloadCurrentLevel;
        MenuManager.OnRestartPressed += ReloadLevel;
        // MenuManager.OnPlayPressed += LoadLobby;
        // NetworkManager.OnRoomCreated += LoadCustomerService;
        NetworkManager.OnRoomJoined += LoadScene;
    }

    public void LoadScene(string _)
    {
        if (GameManager.instance.Role == Roles.CUSTOMER_SERVICE)
        {
            LoadLevel(customerServiceSceneName);
        } else
        {
            LoadLevel(inventoryServiceSceneName);
        }
    }

    private void LoadLevel(string level)
    {
        if (levelLoaded)
        {
            UnloadCurrentLevel();
        }
        // load next
        currentLevel = level;

        if (currentLevel != null)
        {
            SceneManager.LoadScene(currentLevel, LoadSceneMode.Additive);
            levelLoaded = true;
            mainCamera.SetActive(false);
        }
        else
            Debug.Log("Thanks for playing");
        // TODO  Load Final    
    }

    private void UnloadCurrentLevel()
    {
        ClearCurrentScene();
        if (GameManager.instance.Role == Roles.CUSTOMER_SERVICE &&
            SceneManager.GetSceneByName(customerServiceSceneName).isLoaded)
        {
            SceneManager.UnloadSceneAsync(customerServiceSceneName);
        }
        else if (SceneManager.GetSceneByName(inventoryServiceSceneName).isLoaded)
        {
            SceneManager.UnloadSceneAsync(inventoryServiceSceneName);
        }
    }

    private void ClearCurrentScene()
    {
        OnUnload?.Invoke();
        levelLoaded = false;
        mainCamera.SetActive(true);
    }

    /**
     * Pause and Unpause
     */
    public void Pause()
    {

    }

    public void Unpause()
    {

    }

    private void ReloadLevel()
    {
        LoadLevel(currentLevel);
    }
}
