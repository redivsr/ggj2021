﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ServerStatus : MonoBehaviour
{
    [SerializeField] private TMP_Text text;
    [SerializeField] private Image image;

    private void Awake()
    {
        NetworkManager.OnServerConnected += OnConnected;
        NetworkManager.OnServerDisconnected += OnDisconnected;
    }

    private void OnDisconnected(string obj)
    {
        if (image != null)
            image.color = Color.red;
        text.text = "Disconnected";
    }

    private void OnConnected()
    {
        if (image != null)
            image.color = Color.green;
        text.text = "Connected";
    }
}
