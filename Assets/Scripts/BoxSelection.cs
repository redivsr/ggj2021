﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxSelection : MonoBehaviour
{
    [SerializeField] private List<Transform> boxes;
    [SerializeField] private Transform cameraMain;
    [SerializeField] private List<Button> uiButtons;
    [SerializeField] private List<Image> uiBoxImage;

    private int currentSelection = 0;

    public void MoveToBox(int index)
    {
        currentSelection = index;
        SelectBox(index);
    }

    private void SelectBox(int selected)
    {
        Color color = uiBoxImage[selected].color;
        color.a = 1;
        uiBoxImage[selected].color = color;
        for (int index = 0; index < boxes.Count; index++)
        {
            if (index != selected)
            {
                color = uiBoxImage[index].color;
                color.a = 0.3f;
                uiBoxImage[index].color = color;
            }
        }
    }

    private void FixedUpdate()
    {
        if (cameraMain.position.y == boxes[currentSelection].position.y)
        {
            return;
        }
        else
        {
            cameraMain.position = Vector3.MoveTowards(
                cameraMain.position,
                new Vector3(boxes[currentSelection].position.x, boxes[currentSelection].position.y, cameraMain.position.z),
                2
            );
        }
    }
}
