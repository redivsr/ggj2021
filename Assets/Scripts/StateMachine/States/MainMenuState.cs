using UnityEngine;

public class MainMenuState : IState
{
    private MenuManager stateMachine;

    public bool playPressed {get; private set;}

    public MainMenuState(MenuManager stateMachine) {
        this.stateMachine = stateMachine;
        stateMachine.play?.onClick.AddListener(PlayListener);
        stateMachine.exit?.onClick.AddListener(ExitListener);
    }

    public void Tick() {}

    public void OnEnter() {
        playPressed = false;
        // enable main menu panel
        stateMachine.mainMenuPanel.SetActive(true);
        stateMachine.inGameUIPanel.SetActive(false);
        stateMachine.pauseMenuPanel.SetActive(false);
        stateMachine.BackToMainPressed();
        NetworkManager.instance?.LeaveRoom();
        GameManager.instance.gameOver = false;
        PointsController.instance.Clear();
    }

    void PlayListener() {
        if (NetworkManager.instance.isConnected)
            this.playPressed = true;
    }

    void ExitListener() {
        stateMachine.ExitPressed();
    }

    public void OnExit() {
        this.playPressed = false;
        stateMachine.mainMenuPanel.SetActive(false);
    }
}