using System;
using UnityEngine;

public class InLobbyState : IState
{
    public bool RoomJoined;
    public bool BackToMainPressed;
    private MenuManager stateMachine;

    public InLobbyState(MenuManager stateMachine) {
        this.stateMachine = stateMachine;
        BackToMainPressed = false;
        RoomJoined = false;
        stateMachine.joinButton.onClick.AddListener(OnJoinClicked);
        stateMachine.createButon.onClick.AddListener(OnCreateClicked);
        stateMachine.lobbyBackToMainButton.onClick.AddListener(BackToMainMenu);

        NetworkManager.OnRoomCreated += OnRoomCreated;
        NetworkManager.OnRoomJoined += OnRoomJoined;
    }

    public void Tick() { }

    private void BackToMainMenu()
    {
        BackToMainPressed = true;
    }

    public void OnEnter() {
        RoomJoined = false;
        BackToMainPressed = false;
        stateMachine.lobbyMenu.SetActive(true);
        stateMachine.PlayPressed();
    }

    public void OnExit() {
        BackToMainPressed = false;
        stateMachine.lobbyMenu.SetActive(false);
    }

    void OnCreateClicked()
    {
        if (AreFieldsValid())
        {
            if (stateMachine.roleSelected.text.Equals("Customer Service"))
                GameManager.instance.Role = Roles.CUSTOMER_SERVICE;
            else
                GameManager.instance.Role = Roles.INVENTORY;

            NetworkManager.instance.SetNickname(stateMachine.playerName.text);
            NetworkManager.instance.CreateRoom(stateMachine.roomName.text);
        }
    }

    void OnJoinClicked()
    {
        if (AreFieldsValid())
        {

            NetworkManager.instance.SetNickname(stateMachine.playerName.text);
            NetworkManager.instance.JoinRoom(stateMachine.roomName.text);
            if (stateMachine.roleSelected.text.Equals("Customer Service"))
                GameManager.instance.Role = Roles.CUSTOMER_SERVICE;
            else
                GameManager.instance.Role = Roles.INVENTORY;
        }
    }

    private void OnRoomCreated(string roomName)
    {
        RoomJoined = true;
    }


    private void OnRoomJoined(string roomName)
    {
        RoomJoined = true;
    }

    private bool AreFieldsValid()
    {
        return stateMachine.playerName.text != "" || stateMachine.roomName.text != "";
    }
}