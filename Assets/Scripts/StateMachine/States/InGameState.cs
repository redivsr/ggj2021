using UnityEngine;

public class InGameState : IState
{
    public bool escPressed;
    private MenuManager stateMachine;

    public InGameState(MenuManager stateMachine) {
        this.stateMachine = stateMachine;
        escPressed = false;
    }

    public void Tick() {
        if (Input.GetKeyDown(KeyCode.Escape) || GameManager.instance.gameOver) {
            escPressed = true;
        }
    }

    public void OnEnter() {
        escPressed = false;
        stateMachine.inGameUIPanel.SetActive(true);
        stateMachine.PlayPressed();
    }

    public void OnExit() {
        escPressed = false;
        stateMachine.inGameUIPanel.SetActive(false);
    }
}