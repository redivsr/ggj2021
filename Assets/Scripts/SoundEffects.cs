﻿using System.Collections;
using System;
using UnityEngine;

public class SoundEffects : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip buttonPressed;

    private void Awake()
    {
        MenuManager.OnAnyButtonPressed += PlayButtonPressed;
    }

    public void PlayButtonPressed()
    {
        // StartCoroutine(CoroutineButtonPressed());
    }

    private IEnumerator CoroutineButtonPressed()
    {
        audioSource.clip = buttonPressed;
        audioSource.Play();
        yield return null;
    }
}
