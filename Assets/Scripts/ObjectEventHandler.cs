﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectEventHandler : MonoBehaviour
{
    public int inventoryPosition { get; set; }
    private Vector3 mousePosition;
    private Camera cameraMain;
    private bool selected;

    private void Awake()
    {
        cameraMain = Camera.main;
        selected = false;
    }

    private void OnMouseDown()
    {
        selected = !selected;
        // Destroy(gameObject);
    }

    void OnMouseOver()
    {
        // HighlightObject
    }

    private void Update()
    {
        if (selected)
        {
            if (Input.GetMouseButtonUp(0))
            {
                selected = !selected;
                return;
            }
            mousePosition = cameraMain.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(mousePosition.x, mousePosition.y, transform.position.z);
        }
    }
}
