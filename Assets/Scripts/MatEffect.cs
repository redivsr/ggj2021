﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatEffect : MonoBehaviour
{
    [SerializeField] private LayerMask layer;
    [SerializeField] private float force;

    private List<Transform> items;

    private void Awake()
    {
        items = new List<Transform>();
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        foreach(var item in items)
        {
            item.position = Vector3.MoveTowards(
                item.position,
                item.position - new Vector3(force, 0, 0),
                force * Time.deltaTime
            );
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((layer & 1 << collision.gameObject.layer) == 1 << collision.gameObject.layer)
        {
            items.Add(collision.transform);
            Debug.Log("Item added to mat");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        items.Remove(collision.transform);
        Debug.Log("Item removed from mat");
    }
}
