﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoundObjectsManager : MonoBehaviour
{
    [SerializeField] private List<Transform> boxes;

    private Object[] lostObjects;
    private List<int> lostObjectsIndexes;

    #region Singleton
    public static FoundObjectsManager instance;

    void Awake()
    {
        instance = this;
    }
    #endregion

    public int getRandomObjectID()
    {
        int shuffledIndex = Random.Range(0, lostObjectsIndexes.Count);
        int index = lostObjectsIndexes[shuffledIndex];
        lostObjectsIndexes.RemoveAt(shuffledIndex);
        return index;
    }

    //getObjectByID
    public Object getObjectByID(int id)
    {
        return lostObjects[id];
    }

    void Start()
    {
        lostObjects = Resources.LoadAll("FoundObjects", typeof(GameObject));
        lostObjectsIndexes = new List<int>();
        ShuffleObjects();
        AddItemsToBoxes();
    }

    void ShuffleObjects()
    {
        var temp = new List<int>();
        for (int index = 0; index < lostObjects.Length; index++)
        {
            temp.Add(index);
        }

        while (temp.Count != 0)
        {
            var randomIndex = Random.Range(0, temp.Count);
            var randomValue = temp[randomIndex];
            lostObjectsIndexes.Add(randomValue);
            temp.RemoveAt(randomIndex);
        }

        Debug.Log(lostObjectsIndexes);
    }

    void AddItemsToBoxes()
    {
        int itemIndex = 0;
        for (int boxIndex = 0; boxIndex < boxes.Count; boxIndex++)
        {
            for (; itemIndex < lostObjects.Length * (boxIndex + 1) / boxes.Count; itemIndex++)
            {
                var boxLimits = new Vector3(Random.Range(-3.5f, 3.5f), Random.Range(-3.5f, 3.5f), 0);
                InstantiateItem(lostObjectsIndexes[itemIndex], boxes[boxIndex], boxes[boxIndex].position + boxLimits);
            }
        }
    }

    public GameObject InstantiateItem(int pos, Transform parent, Vector3 position)
    {
        var obj = Instantiate(lostObjects[pos], position, Quaternion.identity, parent);
        ((GameObject)obj).GetComponent<ObjectEventHandler>().inventoryPosition = pos;
        return (GameObject)obj;
    }
}
