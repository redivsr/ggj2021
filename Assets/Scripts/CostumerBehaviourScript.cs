﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State 
{
    ARRIVING,
    LEAVING,
    WAITING
}

public class CostumerBehaviourScript : MonoBehaviour
{
    public GameObject speechBubble;
    public float arrivalTime = 100;
    public float waitingTime = 100;
    public AudioSource audioSource;
    public AudioClip[] voices;
    public float speed = 1.0f;
    
    private GameObject lostObject;
    private int lostObjectID;
    private State state;
    //requesting the script that lays on ObjectManager. 
    //Unity should receive the GameObject ObjectManager and extracts the script automatically;
    private FoundObjectsManager foundObjectManager;
    private Vector3 target;

    private bool leaving = false;
    
    //request object
    int request()
    {
        lostObjectID = foundObjectManager.getRandomObjectID();
        lostObject = Instantiate((GameObject)foundObjectManager.getObjectByID(lostObjectID), transform.position + new Vector3(0.5f, 1.3f, 0), Quaternion.identity, transform);
        lostObject.transform.localScale = new Vector3(1f, 1f, 1);
        return lostObjectID;
    }

    // Complain
    void complain()
    {
        state = State.LEAVING;
        StartCoroutine(PlayOneShot(voices[0]));
        PointsController.instance.AddError();
    }

    // Happy when found the lost object
    void happy()
    {
        state = State.LEAVING;
        StartCoroutine(PlayOneShot(voices[1]));
        PointsController.instance.AddHit();
    }

    private IEnumerator PlayOneShot(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
        yield return null;
    }

    // Arrive
    void arrive()
    {
        if (arrivalTime >= 0)
        {
            arrivalTime -= Time.deltaTime;
            return;
        }
        show();
        request();
        state = State.WAITING;
    }

    // Leave
    void leave()
    {
        state = State.LEAVING;
        if (!leaving)
        {
            leaving = true;
            speechBubble.SetActive(false);
            Destroy(lostObject);
            gameObject.GetComponent<Collider2D>().enabled = false;
        }

        move(Time.deltaTime);
    }

    void wait() {
        waitingTime -= Time.deltaTime;
        //If waiting time has ended costumer complains and leave
        if (waitingTime <= 0)
        {
            complain();
            state = State.LEAVING;
        }
    }

    // Move left
    void move(float deltaTime)
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target, step);
    }

    void hide()
    {
        gameObject.GetComponent<Renderer>().enabled = false;
        if (lostObject)
            lostObject.GetComponent<Renderer>().enabled = false;
    }

    void show()
    {
        speechBubble.SetActive(true);
        gameObject.GetComponent<Renderer>().enabled = true;
        if (lostObject)
            lostObject.GetComponent<Renderer>().enabled = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        foundObjectManager = FoundObjectsManager.instance;
        target = new Vector3 (1000, 0, 0);
        state = State.ARRIVING;
        hide();        
    }

    // Update is called once per frame
    void Update()
    {
        switch(state)
        {
            case State.ARRIVING:
                arrive();
                break;
            case State.LEAVING:
                leave();
                break;
            case State.WAITING:
                wait();
                break;
        }       
    }

    void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.layer == 8) //items layer is 8
        {
            int otherID = other.gameObject.GetComponent<ObjectEventHandler>().inventoryPosition;
            if (lostObjectID == otherID)
            {
                if (!leaving)
                {
                    happy();
                }
            }
            else
            {
                if (!leaving)
                {
                    complain();
                }
            }
            state = State.LEAVING;
            other.gameObject.GetComponent<ObjectEventHandler>().enabled = false;
            Destroy(other.gameObject);
        }
    }

    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
