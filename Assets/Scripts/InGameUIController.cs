﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class InGameUIController : MonoBehaviour
{
    [SerializeField] private Slider life;
    [SerializeField] private TMP_Text mana;
    [SerializeField] private TMP_Text coins;

    void Awake() {
        // TODO LifeController.OnLifeChange += SetLife;
        // TODO ManaController.OnManaChange += SetMana;
        // TODO CoinController.OnCoinChange += SetCoins;
        SetLife(0.5f);
    }
    public void SetLife(float life) {
        this.life.value = life;
    }

    public void SetCoins(int coin) {
        this.coins.text = coin.ToString();
    }

    public void SetMana(int mana) {
        this.mana.text = mana.ToString();
    }
}
