﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReceiveItems : MonoBehaviour
{
    [SerializeField] private List<Transform> slots;

    void Start()
    {
        GameManager.OnItemReceived += ItemReceived;
    }

    private int GetFirstFreeSlot()
    {
        for (int index = 0; index < slots.Count; index++)
        {
            if (slots[index].childCount == 0) return index;
        }
        return -1;
    }

    private void ItemReceived(int obj)
    {
        int index = GetFirstFreeSlot();
        if (index != -1 && !isDuplicated(obj))
        {
            // Object item = FoundObjectsManager.instance.getObjectByID(obj);
            GameObject item = FoundObjectsManager.instance.InstantiateItem(obj, slots[index], slots[index].position);
            item.transform.localScale = new Vector3(1.2f, 1.2f, 1);
        }
    }

    private bool isDuplicated(int index)
    {
        int i = 0;
        for (; i < slots.Count; i++)
        {
            if (slots[i].childCount > 0 && slots[i].GetComponentInChildren<ObjectEventHandler>().inventoryPosition == index)
            {
                return true;
            }
        }
        return false;
    }
}
