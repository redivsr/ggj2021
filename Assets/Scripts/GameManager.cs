using System;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public bool GamePaused { get; private set; }
    public Roles Role { get; set; }

    public static event Action<int> OnItemReceived;

    #region Singleton
    public static GameManager instance;

    public bool gameOver = false;

	void Awake ()
	{
        if (instance != null && instance != this)
        {
            gameObject.SetActive(false);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
#endregion

    public void Start() {
        MenuManager.OnPlayPressed += Unpause;
        MenuManager.OnPausePressed += Pause;
        MenuManager.OnExitPressed += ExitApplication;
        MenuManager.OnRestartPressed += RestartLevel;
        MenuManager.OnBackToMainPressed += Unpause;
        NetworkManager.OnMessageReceived += OnNetwrokEventReceived;
        NetworkManager.OnSecondPlayerEnteredRoom += SecondPlayerEntered;

        PointsController.OnHitsChanged += OnHitsChanged;
        PointsController.OnErrorsChanged += OnErrorsChanged;
        PointsController.OnStarsChanged += OnStarsChanged;

        // Pause();
    }

    private void OnStarsChanged(int value)
    {
        // NetworkManager.instance.SendEvent((byte)NetworkEventType.STARS, value);
    }

    private void OnErrorsChanged(int value)
    {
        if (Role == Roles.CUSTOMER_SERVICE)
            NetworkManager.instance.SendEvent((byte)NetworkEventType.ERRORS, value);
    }

    private void OnHitsChanged(int value)
    {
        if (Role == Roles.CUSTOMER_SERVICE)
            NetworkManager.instance.SendEvent((byte)NetworkEventType.HITS, value);
    }

    /**
     * Pause and Unpause
     */
    public void Pause() {
        GamePaused = true;
        Time.timeScale = 0;

        Debug.Log("Game Paused");
    }

    public void Unpause() {
        Time.timeScale = 1;
        GamePaused = false;

        Debug.Log("Game Unpaused");
    }

    private void RestartLevel()
    {
        // Clean data
    }

    public void ExitApplication() {
        Debug.Log("Exiting application");
        // Are you sure?
        // Save data if needed?
        Application.Quit();
    }

    private void SecondPlayerEntered(string nickname)
    {
        Debug.Log("Second player entered " + nickname);
        NetworkManager.instance.SendEvent((byte) NetworkEventType.START, 1);
    }

    public void OnNetwrokEventReceived(NetworkEventType eventType, int data)
    {
        switch (eventType)
        {
            case NetworkEventType.START:
                Unpause();
                break;
            case NetworkEventType.PAUSE:
                Pause();
                break;
            case NetworkEventType.UNPAUSE:
                Unpause();
                break;
            case NetworkEventType.CLOSE:
                Pause();
                // TODO show result
                break;
            case NetworkEventType.ITEM_SENT:
                Debug.Log(data.ToString());
                if (Role == Roles.CUSTOMER_SERVICE)
                    OnItemReceived?.Invoke(data);
                break;
            case NetworkEventType.STARS:
                // PointsController.instance.errors = data;
                break;
            case NetworkEventType.HITS:
                if (Role == Roles.INVENTORY)
                    PointsController.instance.AddHit();
                break;
            case NetworkEventType.ERRORS:
                if (Role == Roles.INVENTORY)
                    PointsController.instance.AddError();
                break;
            default:
                break;
        }
    }
}

public enum Roles
{
    INVENTORY,
    CUSTOMER_SERVICE
}