using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {
    [Header("Menu Panels")]
    [SerializeField] public GameObject mainMenuPanel;
    [SerializeField] public GameObject pauseMenuPanel;
    [SerializeField] public GameObject inGameUIPanel;
    [SerializeField] public GameObject lobbyMenu;

    [Header("Main Menu Buttons")]
    [SerializeField] public Button play;
    [SerializeField] public Button credits;
    [SerializeField] public Button exit;

    [Header("Pause Buttons")]
    [SerializeField] public Button continueButton;
    [SerializeField] public Button backToMainButton;
    [SerializeField] public Button restartButton;

    [Header("Lobby Items")]
    [SerializeField] public Button joinButton;
    [SerializeField] public Button createButon;
    [SerializeField] public Button lobbyBackToMainButton;
    [SerializeField] public TMP_InputField playerName;
    [SerializeField] public TMP_InputField roomName;
    [SerializeField] public TMP_Text roleSelected;


    public static event Action OnPlayPressed;
    public static event Action OnExitPressed;
    public static event Action OnPausePressed;
    public static event Action OnBackToMainPressed;
    public static event Action OnRestartPressed;
    public static event Action OnAnyButtonPressed;

    private StateMachine stateMachine;
    private InGameState inGame;
    private MainMenuState mainMenu;
    private PausedState paused;
    private InLobbyState inLobby;

    void Start() {
        stateMachine = new StateMachine();

        inGame = new InGameState(this);
        mainMenu = new MainMenuState(this);
        paused = new PausedState(this);
        inLobby = new InLobbyState(this);

        stateMachine.AddTransition(mainMenu, inLobby, () => mainMenu.playPressed);
        stateMachine.AddTransition(inLobby, inGame, () => inLobby.RoomJoined);
        stateMachine.AddTransition(inGame, paused, () => inGame.escPressed);
        // stateMachine.AddTransition(inGame, paused, () => GameManager.instance.gameOver);
        stateMachine.AddTransition(paused, inGame, () => paused.unpause);
        stateMachine.AddTransition(paused, mainMenu, () => paused.backToMain);
        stateMachine.AddTransition(inLobby, mainMenu, () => inLobby.BackToMainPressed);

        stateMachine.SetState(mainMenu);
    }

    private void Update () {
        stateMachine.Tick();
    }

    public void BackToMainPressed() {
        OnAnyButtonPressed?.Invoke();
        OnBackToMainPressed?.Invoke();
    }

    public void PlayPressed() {
        // OnAnyButtonPressed?.Invoke();
        OnPlayPressed?.Invoke();
    }

    public void PausePressed() {
        OnAnyButtonPressed?.Invoke();
        OnPausePressed?.Invoke();
    }

    public void RestartPressed()
    {
        OnAnyButtonPressed?.Invoke();
        OnRestartPressed?.Invoke();
    }

    public void ExitPressed() {
        // TODO Are you sure?
        OnAnyButtonPressed?.Invoke();
        OnExitPressed?.Invoke();
    }
}
