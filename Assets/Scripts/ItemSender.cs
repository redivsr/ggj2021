﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSender : MonoBehaviour
{
    private Queue<GameObject> itemsToSend;

    private void Awake()
    {
        itemsToSend = new Queue<GameObject>();
    }

    private void FixedUpdate()
    {
        if (itemsToSend.Count > 0)
        {
            GameObject item = itemsToSend.Dequeue();
            NetworkManager.instance.SendEvent(
                (byte) NetworkEventType.ITEM_SENT, 
                (object) item.GetComponent<ObjectEventHandler>().inventoryPosition
            );
            Destroy(item);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (itemsToSend.Contains(collision.gameObject)) return;

        itemsToSend.Enqueue(
            collision.gameObject
        );
    }
}
