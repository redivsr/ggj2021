﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    public static event Action OnServerConnected;
    public static event Action<string> OnRoomCreated;
    public static event Action<string> OnRoomJoined;
    public static event Action<string> OnServerDisconnected;
    public static event Action<string> OnSecondPlayerEnteredRoom;
    public static event Action<NetworkEventType, int> OnMessageReceived;

    public static NetworkManager instance;

    private string roomName;
    public bool isConnected { get; private set; }

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            gameObject.SetActive(false);
        } else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            isConnected = false;
        }
    }

    private TypedLobby customLobby = new TypedLobby("customLobby", LobbyType.Default);

    private Dictionary<string, RoomInfo> cachedRoomList = new Dictionary<string, RoomInfo>();

    public void JoinLobby()
    {
        PhotonNetwork.JoinLobby(customLobby);
    }

    private void UpdateCachedRoomList(List<RoomInfo> roomList)
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            RoomInfo info = roomList[i];
            if (info.RemovedFromList)
            {
                cachedRoomList.Remove(info.Name);
            }
            else
            {
                cachedRoomList[info.Name] = info;
            }
        }
    }


    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        UpdateCachedRoomList(roomList);
    }

    private void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        isConnected = true;
        base.OnConnectedToMaster();
        OnServerConnected?.Invoke();
        Debug.Log("Connected to master server");
    }

    public void SetNickname(string nickname)
    {
        PhotonNetwork.NickName = nickname;
    }

    public void CreateRoom(string roomName)
    {
        // roomName = CreateRandomRoomName(5);

        PhotonNetwork.CreateRoom(roomName);
    }

    public void JoinRoom(string roomName)
    {
        Debug.Log(PhotonNetwork.NetworkingClient.RoomsCount);
        LeaveRoom();
        this.roomName = roomName;
        PhotonNetwork.JoinRoom(roomName);
        Debug.Log("joining room " + roomName);
    }

    public void LeaveRoom()
    {
        if (PhotonNetwork.InRoom)
        {
            PhotonNetwork.LeaveRoom();
        }
    }

    public override void OnLeftRoom()
    {
        base.OnLeftRoom();
        Debug.Log("Left room");
        PhotonNetwork.Disconnect();
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log(message);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log(message);
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        OnRoomCreated?.Invoke(this.roomName);
        Debug.Log("Room created " + this.roomName);
        PhotonNetwork.NetworkingClient.EventReceived += OnNetworkMessageReceived;
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log(newPlayer.NickName);
        OnSecondPlayerEnteredRoom?.Invoke(newPlayer.NickName);
        // SendEvent(1, new object[1] { "1" } );
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        OnRoomJoined?.Invoke(this.roomName);
        PhotonNetwork.NetworkingClient.EventReceived += OnNetworkMessageReceived;
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        OnServerDisconnected?.Invoke(cause.ToString());
        isConnected = false;
        PhotonNetwork.ConnectUsingSettings();
    }

    private void OnDestroy()
    {
        if (PhotonNetwork.IsConnected)
            PhotonNetwork.Disconnect();
    }

    public void SendEvent(byte eventCode, object data)
    {
        PhotonNetwork.NetworkingClient?.OpRaiseEvent(eventCode, data, RaiseEventOptions.Default, SendOptions.SendReliable);
    }

    private void OnNetworkMessageReceived(EventData obj)
    {
        OnMessageReceived?.Invoke((NetworkEventType)obj.Code, obj.CustomData != null ? (int)obj.CustomData : -1);
    }

    private string CreateRandomRoomName(int stringLength = 10)
    {
        int _stringLength = stringLength - 1;
        string randomString = "";
        string[] characters = new string[] { "a", "b", "c", "d", "e", "f", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        for (int i = 0; i <= _stringLength; i++)
        {
            randomString = randomString + characters[UnityEngine.Random.Range(0, characters.Length)];
        }
        return randomString;
    }
}

public enum NetworkEventType
{
    ITEM_SENT,
    START,
    PAUSE,
    UNPAUSE,
    CLOSE,
    HITS,
    ERRORS,
    STARS
}