﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCSpawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> npcs;
    [SerializeField] private List<Transform> slots;
    [SerializeField] private int numberOfNPCs = 10;
    [SerializeField] private float spawnTime = 30f;

    private float previousSpawnedTime;
    private int npcsSpawned = 0;

    // Start is called before the first frame update
    void Start()
    {
        previousSpawnedTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (numberOfNPCs > 0 && npcsSpawned >= numberOfNPCs)
        {
            // TODO Game is over
            for (int index = 0; index < slots.Count; index++)
            {
                if (slots[index].childCount > 0) return;
            }

            GameManager.instance.gameOver = true;
            // GameManager.instance.Pause();
            return;
        }

        if (Time.time - spawnTime > previousSpawnedTime)
        {
            InstantiateNpc();
            previousSpawnedTime = Time.time;
        }
    }

    private int GetFirstFreeSlot()
    {
        for (int index = 0; index < slots.Count; index++)
        {
            if (slots[index].childCount == 0) return index;
        }
        return -1;
    }

    public void InstantiateNpc()
    {
        int index = GetFirstFreeSlot();
        if (index != -1)
        {
            Instantiate(
                npcs[Random.Range(0, npcs.Count)], 
                slots[index].position, 
                Quaternion.identity, 
                slots[index]
            );
            npcsSpawned++;
        }
    }
}
