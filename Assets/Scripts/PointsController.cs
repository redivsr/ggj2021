﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PointsController : MonoBehaviour
{
    public static event Action<int> OnStarsChanged;
    public static event Action<int> OnHitsChanged;
    public static event Action<int> OnErrorsChanged;

    public static PointsController instance;

    private void Awake()
    {
        instance = this;
    }

    private int _stars;
    public int stars 
    {
        get => _stars;
        set {
            _stars = value;
            OnStarsChanged?.Invoke(_stars);
        }
    }

    private int _hits;
    public int hits
    {
        get => _hits;
        set
        {
            _hits = value;
            OnHitsChanged?.Invoke(_hits);
            if (_errors + _hits > 0)
                stars = Mathf.Clamp(_hits * 5 / (_errors + _hits), 0, 5);
            else
                stars = 0;
        }
    }

    private int _errors;
    public int errors
    {
        get => _errors;
        set
        {
            _errors = value;
            OnErrorsChanged?.Invoke(_errors);
            if (_errors + _hits > 0)
                stars = Mathf.Clamp(_hits * 5 / (_errors + _hits), 0, 5);
            else
                stars = 0;
        }
    }

    public void AddHit()
    {
        hits++;
    }

    public void AddError()
    {
        errors++;
    }

    public void Clear()
    {
        stars = 0;
        errors = 0;
        hits = 0;
    }
}
