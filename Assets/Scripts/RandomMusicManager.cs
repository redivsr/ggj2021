﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMusicManager : MonoBehaviour
{
    public AudioSource audioSource;
    public AudioClip[] notes;

    public float timeBetweenShots = 0.25f;
    float timer;

    AudioClip RandomClip()
    {
        return notes[Random.Range(0, notes.Length)];
    }

    // Start is called before the first frame update
    void Start()
    {
                
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer > timeBetweenShots)
        {
            audioSource.PlayOneShot(RandomClip());
            timer = 0;
        }
    }
}
