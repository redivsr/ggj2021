﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIPoints : MonoBehaviour
{
    [SerializeField] private List<Image> stars;
    [SerializeField] private TMP_Text hits;
    [SerializeField] private TMP_Text errors;

    // Start is called before the first frame update
    void Start()
    {
        PointsController.OnStarsChanged += OnStarsChanged;
        PointsController.OnHitsChanged += OnHitsChanged;
        PointsController.OnErrorsChanged += OnErrorsChanged;
    }

    public void OnErrorsChanged(int value)
    {
        if (errors !=null)
            errors.text = value.ToString();
    }

    public void OnHitsChanged(int value)
    {
        if (hits != null)
            hits.text = value.ToString();
    }

    public void OnStarsChanged(int number)
    {
        int index = 0;
        for (; index < number; index++)
        {
            stars[index].color = Color.yellow;
        }

        for(; index < stars.Count; index++)
        {
            stars[index].color = Color.gray;
        }
    }
}
